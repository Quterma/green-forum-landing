const registrationFormSubmitButton = document.querySelector('.js-registration-form-submit');

const showConfirmation = () => {
	document.body.classList.add('onConfirmationPopup');
	window.scrollTo(0, 0);
};

registrationFormSubmitButton.addEventListener('click', showConfirmation);
