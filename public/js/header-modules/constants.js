const constants = {};

// Logo Image paths
constants.logo = {
	logoTwoLineWhiteSrc: './assets/images/logo/logo-two-line-white.png',
	logoTwoLineGreenSrc: './assets/images/logo/logo-two-line-green.png',
	logoOneLineWhiteSrc: './assets/images/logo/logo-one-line-white.png',
};

constants.topHeaderMenuButtonImage = {
	burger: './assets/images/header/burger.png',
	commonHeaderBurger: './assets/images/header/common-header-burger.png',
	cross: './assets/images/popup/cross.png',
};

export default constants;
