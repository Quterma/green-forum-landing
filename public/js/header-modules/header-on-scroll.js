import constants from './constants.js';

const topHeaderLogoElement = document.querySelector('.js-topHeaderLogo');
const scrollable = document.querySelector('.js-header-scrollable');

export const addScrolledStyles = () => {
	document.body.classList.add('onScrolled');
	topHeaderLogoElement.src = constants.logo.logoOneLineWhiteSrc;
};

export const removeScrolledStyles = () => {
	document.body.classList.remove('onScrolled');
	topHeaderLogoElement.src = constants.logo.logoTwoLineWhiteSrc;
};

// Function for changing styles on scroll
export const handleHeaderOnDocumentScroll = () => {
	if (!scrollable) return;

	if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
		addScrolledStyles();
	} else {
		removeScrolledStyles();
	}
};
