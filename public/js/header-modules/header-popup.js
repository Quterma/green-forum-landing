import constants from './constants.js';

const topHeaderLogoElement = document.querySelector('.js-topHeaderLogo');
const topHeaderMenuButtonImageElement = document.querySelector('.js-headerMenuButtonImage');

export const showPopup = () => {
	document.body.classList.add('onPopup');
	topHeaderLogoElement.src = constants.logo.logoTwoLineGreenSrc;
	topHeaderMenuButtonImageElement.src = constants.topHeaderMenuButtonImage.cross;

	return true;
};

export const hidePopup = () => {
	document.body.classList.remove('onPopup');

	const isCommon = [...topHeaderMenuButtonImageElement.classList].includes('common-header-burger');

	topHeaderLogoElement.src = isCommon ? constants.logo.logoTwoLineGreenSrc : constants.logo.logoTwoLineWhiteSrc;
	topHeaderMenuButtonImageElement.src = isCommon
		? constants.topHeaderMenuButtonImage.commonHeaderBurger
		: constants.topHeaderMenuButtonImage.burger;

	return false;
};
