import { showPopup, hidePopup } from './header-modules/header-popup.js';
import { removeScrolledStyles, handleHeaderOnDocumentScroll } from './header-modules/header-on-scroll.js';

const headerPopupButton = document.querySelector('.js-headerMenuButton');

const addListeners = async () => {
	let isPopup = false;

	window.addEventListener('scroll', handleHeaderOnDocumentScroll);

	const hide = () => {
		window.addEventListener('scroll', handleHeaderOnDocumentScroll);
		return hidePopup();
	};

	const show = () => {
		removeScrolledStyles();
		window.removeEventListener('scroll', handleHeaderOnDocumentScroll);
		return showPopup();
	};

	headerPopupButton.addEventListener('click', () => {
		isPopup = isPopup ? hide() : show();
	});
};

addListeners();
